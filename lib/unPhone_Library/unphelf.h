// unphelf.h
// definitions shared by both unPhone and WaterElf

#ifndef UNPHELF_H
#define UNPHELF_H

#include "joinme.h"             // OTA etc.
#include <stdint.h>             // integer types

// MAC address //////////////////////////////////////////////////////////////
char *getMAC(char *buf);
extern char MAC_ADDRESS[13]; // MACs are 12 chars, plus the NULL terminator

#endif
