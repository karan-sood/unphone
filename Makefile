# unPhone builds

SHELL=/bin/bash
ECHO=echo -e "\033[0;32m"
ECHON=echo -e "\033[0m"
unexport CDPATH # don't echo directories when cd'ing
ESPPORT := $(shell [ ! -z "$$ESPPORT" ] && echo $$ESPPORT || \
  for p in ttyUSB0 ttyUSB1 cu.SLAB_USBtoUART; do \
  [ -r /dev/$${p} ] && echo /dev/$${p} && break; done)

SDKS_DIR=$(CURDIR)/sdks
ARD_LIB_DIR=$(SDKS_DIR)/Arduino/libraries
ARD_HW_DIR=$(SDKS_DIR)/Arduino/hardware/espressif

#ESP_IDF_VERSION=v3.1.3
# the AsyncTCP lib we have currently only works with HEAD commits
ESP_IDF_VERSION=abea9e4
#ESP_ARDUINO_VERSION=1.0.1
ESP_ARDUINO_VERSION=5af0336

ESP_IDF_DIR=$(SDKS_DIR)/esp-idf
export IDF_PATH=$(ESP_IDF_DIR)
ESP_ARDUINO_DIR=$(SDKS_DIR)/arduino-esp32
ESP_GCC_DIR=$(ESP_ARDUINO_DIR)/tools/xtensa-esp32-elf
ESP_CORE_DIR=$(ESP_ARDUINO_DIR)

# arduino IDE 1.8.5
#ARDUINO_IDE_VER=1.8.5
#ARDUINO_IDE_DIR=
#ARDUINO_IDE_RUNNER_VER=5
#ARDUINO_IDE_SDKS_DIR=$(SDKS_DIR)/arduino-$(ARDUINO_IDE_VER)

# arduino IDE 1.8.9
ARDUINO_IDE_VER=1.8.9
ARDUINO_IDE_DIR=
ARDUINO_IDE_RUNNER_VER=8
ARDUINO_IDE_SDKS_DIR=$(SDKS_DIR)/arduino-$(ARDUINO_IDE_VER)

# arduino IDE 1.9.0-beta
#ARDUINO_IDE_VER=1.9.0-beta
#ARDUINO_IDE_DIR=ide_beta/
#ARDUINO_IDE_RUNNER_VER=9
#ARDUINO_IDE_SDKS_DIR=$(SDKS_DIR)/arduino-PR-beta1.9-BUILD-107

# arduino IDE download page
ARDUINO_IDE_FILE=arduino-$(ARDUINO_IDE_VER)-linux64.tar.xz
ARDUINO_IDE_URL=https://downloads.arduino.cc/$(ARDUINO_IDE_DIR)$(ARDUINO_IDE_FILE)

help:
	@echo 'Makefile for unPhone                                          '
	@echo '                                                              '
	@echo 'Targets:                                                      '
	@echo '   setup         install ESP IDF & Arduino IDE & libraries    '
	@echo '                                                              '
	@echo '   esp-support   set up ESP IDF build environment in sdks/    '
	@echo '   arduino-libs  (re)generate sdks/Arduino                    '
	@echo '   arduino-ide   get or run the Arduino IDE (linux x64 only!) '
	@echo '                                                              '
	@echo '   incoming-libs download latest lib versions (and patch)     '
	@echo '   check-incoming-libs  diff latest vs. current               '

setup: esp-support arduino-libs arduino-ide
	@$(ECHO)"\nsetup done\n"

# patched libraries for the unphone board
arduino-libs:
	@$(ECHO) preparing libraries; $(ECHON)
	mkdir -p $(ARD_HW_DIR); \
        ( [ -r $(ARD_HW_DIR)/esp32 ] || \
          ( cd $(ARD_HW_DIR) && ln -s $(ESP_CORE_DIR) esp32; ) )
	[ -d $(ARD_LIB_DIR) ] || cp -a lib $(ARD_LIB_DIR)
	bin/lib-injector.sh
incoming-libs:
	@$(ECHO)"\npulling down incoming-libs...\n"; $(ECHON)
	bin/lib-injector.sh -u
check-incoming-libs:
	@$(ECHO)"\ndiffing incoming-libs...\n"; $(ECHON)
	cd incoming-libs && for d in *; do \
          $(ECHO)'\n'$$d; $(ECHON); \
          diff -wqr $$d ../lib/$$d |grep -v 'identical'; done
check-lmic:
	cd $(ARD_LIB_DIR)/arduino-lmic/src && \
        for f in `find . -type f`; do \
          diff $$f $(CURDIR)/lib-archive/vanilla-lmic/src/$$f; done

# arduino layer as per
# https://github.com/espressif/arduino-esp32/blob/master/docs/esp-idf_component.md
arduino-esp32:
	@$(ECHO)"\nsetting up Arduino components...\n"
	@$(ECHON)
	mkdir -p $(SDKS_DIR)
	cd $(SDKS_DIR) && [ -d $(ESP_ARDUINO_DIR) ] || ( \
          git clone \
            https://github.com/espressif/arduino-esp32.git && \
          cd $(ESP_ARDUINO_DIR) && \
          git checkout $(ESP_ARDUINO_VERSION) && \
          git submodule update --init --recursive && \
          cd tools && python3 get.py \
        )
	@$(ECHO)"\nArduino components done\n"

# IDF etc. setup, as per
# https://docs.espressif.com/projects/esp-idf/en/stable/get-started/linux-setup.html
esp-support: arduino-esp32 setup-idf esp-pip-installs
	@$(ECHO)"\nesp-support done\n"

setup-idf:
	@$(ECHO)"\nsetting up IDF...\n"
	@$(ECHO)"\nmake sure you install IDF prerequisites, e.g.:"
	@$(ECHON)
	@echo sudo apt-get install gcc git wget make libncurses-dev \
          flex bison gperf python python-serial
	@$(ECHO)"\ndownloading compiler toolchain...\n"
	@$(ECHON)
	@$(ECHO)"\nsetting up $${IDF_PATH}...\n"
	@$(ECHON)
	cd $(SDKS_DIR) && [ -d $(ESP_IDF_DIR) ] || ( \
          git clone \
            https://github.com/espressif/esp-idf.git && \
          cd $(ESP_IDF_DIR) && \
          git checkout $(ESP_IDF_VERSION) && \
          git submodule update --init --recursive \
        )
	@$(ECHO)"\nIDF install at $${IDF_PATH} should be usable BUT:"
	@$(ECHO)"you must set IDF_PATH to $${IDF_PATH} in your environment"
	@$(ECHO)"and add $(ESP_GCC_DIR)/bin to your PATH\n"
	@$(ECHO)"(alternatively create a link from ~/esp and use that)\n"

# update python packages needed by IDF
esp-pip-installs:
	python -m pip install --user -r $(IDF_PATH)/requirements.txt

# report the commit hashes of the IDF and ESP arduino core clones
esp-commit-hashes:
	@cd $(ESP_IDF_DIR) && pwd && git log --pretty=format:'%h' -n 1
	@cd $(ESP_ARDUINO_DIR) && pwd && git log --pretty=format:'%h' -n 1

# arduino IDE (1.8.5)
# TODO support other platforms like https://pastebin.com/gT1jfNNq
arduino-ide:
	ARDUINO_IDE_VERNAME=`echo $(ARDUINO_IDE_VER) |sed 's,\.,,g'` && \
        ARDUINO_DOT_DIR=$(SDKS_DIR)/dot-arduino15-$$ARDUINO_IDE_VERNAME && \
	[ -d $$ARDUINO_DOT_DIR ] || ( \
          mkdir $$ARDUINO_DOT_DIR && \
          echo "sketchbook.path=$(SDKS_DIR)/Arduino" > \
          $$ARDUINO_DOT_DIR/preferences.txt; \
        ); \
	if [ -d $(ARDUINO_IDE_SDKS_DIR) ]; then \
          $(ECHO) "\nrunning Arduino IDE"; $(ECHON); \
          bin/arduino-ide-runner.sh -$(ARDUINO_IDE_RUNNER_VER); \
        else \
          $(ECHO) "\ndownloading Arduino IDE"; $(ECHON); \
          cd $(SDKS_DIR) && wget $(ARDUINO_IDE_URL) && \
          tar xJf $(ARDUINO_IDE_FILE) && rm $(ARDUINO_IDE_FILE); \
        fi
	@$(ECHO) "done"; $(ECHON)

# burn a .bin
burn:
	python \
        $(SDKS_DIR)/esp-idf/components/esptool_py/esptool/esptool.py \
        --chip esp32 --port /dev/ttyUSB0 --baud 921600 \
        --before default_reset --after hard_reset write_flash -z \
        --flash_mode dio --flash_freq 40m \
        --flash_size detect 0x10000 \
        my-sketch.ino.bin

.PHONY: incoming-libs
