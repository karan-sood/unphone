// main.cpp

#include "waterelf.h"
#include "private.h" // stuff not for checking in

#include <pins_arduino.h>
#include <Arduino.h>
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include <FS.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>
#include <ESPmDNS.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>

// OTA support //////////////////////////////////////////////////////////////
int firmwareVersion = 20; // keep up-to-date! (used to check for updates)

// we're not in arduino land any more, so need to declare all function protos
void blink(int times);
void handleActuate(AsyncWebServerRequest *request);
void handleAlgchz(AsyncWebServerRequest *request);
void handleAnalogconf(AsyncWebServerRequest *request);
void handleData(AsyncWebServerRequest *request);
void handleElfstatus(AsyncWebServerRequest *request);
void handleNotFound(AsyncWebServerRequest *request);
void handlePHchz(AsyncWebServerRequest *request);
void handlePHconf(AsyncWebServerRequest *request);
void handleReboot(AsyncWebServerRequest *request);
void handleRoot(AsyncWebServerRequest *request);
void handleServerconf(AsyncWebServerRequest *request);
void handleSvrchz(AsyncWebServerRequest *request);
void handleValve1(AsyncWebServerRequest *request);
void handleValve2(AsyncWebServerRequest *request);
void handleValve3(AsyncWebServerRequest *request);
void handleValve(AsyncWebServerRequest *request, int valveNum);
void handleWfchz(AsyncWebServerRequest *request);
void handleWifi(AsyncWebServerRequest *request);
// void footAndRespond(String toSend, AsyncWebServerRequest *request);
byte hextoi(char c);
void startPeripherals();
void startWebServer();
void getAnalog(float* a);
void getHumidity(float* airCelsius, float* airHumid);
void getLevel(int echoPin, long* waterLevel);
void getLight(uint16_t* lux);
void getPH(float* pH);
void getTemperature(float* waterCelsius);
void printIPs();

/////////////////////////////////////////////////////////////////////////////
// SPIFFS editor (ace.js) stuff /////////////////////////////////////////////
#include <SPIFFSEditor.h>
const char* SPIFFS_username = "some user name";
const char* SPIFFS_password = "some pw";

/////////////////////////////////////////////////////////////////////////////
// resource management stuff ////////////////////////////////////////////////
const char *elfVersion = "2.0";
String elfVersionStr = String(elfVersion);
int loopCounter = 0;
const int LOOP_ROLLOVER = 100000; // how many loops per action slice
const int TICK_MONITOR = 0;
const int TICK_WIFI_DEBUG = 500;
const int TICK_POST_DEBUG = 200;
const int TICK_HEAP_DEBUG = 100000;
bool reboot=false;  

// MAC address //////////////////////////////////////////////////////////////
char MAC_ADDRESS[13]; // MAC addresses are 12 chars, plus the NULL terminator

// serial line speed
#ifndef ELF_SERIAL_SPEED
#define ELF_SERIAL_SPEED 115200
#endif

/////////////////////////////////////////////////////////////////////////////
// wifi management stuff ////////////////////////////////////////////////////
AsyncWebServer webServer(80);
String apSSIDStr = "WaterElf-" + String(getMAC(MAC_ADDRESS));
const char* apSSID = apSSIDStr.c_str();
String svrAddr = ""; // loaded from config.txt


/////////////////////////////////////////////////////////////////////////////
// page generation stuff ////////////////////////////////////////////////////
String pageTopStr = String(
  "<html><head><title>WaterElf Aquaponics Helper [ID: " + apSSIDStr + "]"
);
const char* pageTop = pageTopStr.c_str();
const char* pageTop2 = "</title>\n"
  "<meta charset=\"utf-8\">"
  "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
  "<style>body{background:#FFF;color: #000;font-family: sans-serif;font-size: 150%;}</style>"
  "</head><body>\n";
const char* pageDefault = // TODO build the growbeds according to their num
  "<h2>Welcome to WaterElf</h2>\n"
  "<h2>Control</h2>\n"
  "<p><ul>\n"
  "<li><a href='/wifi'>Join a wifi network</a></li>\n"
  "<li><a href='/serverconf'>Configure data sharing</a></li>\n"
  "<li><a href='/analogconf'>Configure analog sensor</a></li>\n"
  "<li><a href='/pHconf'>Configure & calibrate pH sensor</a></li>\n"

  "<li>"
    "<form method='POST' action='valve1'>\n"
    "Growbed 1: "
    "Air pump ON <input type='radio' name='state' value='Pon'>\n"
    "Air exhaust ON <input type='radio' name='state' value='Eon'>\n"
    "Both OFF <input type='radio' name='state' value='Boff'>\n"
    "<input type='submit' value='Submit'></form>\n"
  "</li>"
  "<li>"
    "<form method='POST' action='valve2'>\n"
    "Growbed 2: "
    "Air pump ON <input type='radio' name='state' value='Pon'>\n"
    "Air exhaust ON <input type='radio' name='state' value='Eon'>\n"
    "Both OFF <input type='radio' name='state' value='Boff'>\n"
    "<input type='submit' value='Submit'></form>\n"
  "</li>"
  "<li>"
    "<form method='POST' action='valve3'>\n"
    "Growbed 3: "
    "Air pump ON <input type='radio' name='state' value='Pon'>\n"
    "Air exhaust ON <input type='radio' name='state' value='Eon'>\n"
    "Both OFF <input type='radio' name='state' value='Boff'>\n"
    "<input type='submit' value='Submit'></form>\n"
  "</li>"
  "<li>\n"
    "<form method='POST' action='actuate'>\n"
    "Power: "
    "on <input type='radio' name='state' value='on'>\n"    "off <input type='radio' name='state' value='off'>\n"
    "<input type='submit' value='Submit'></form>\n"
  "</li>"
  "</ul></p>\n"
  "<h2>Monitor</h2>\n"
  "<p><ul>\n"
  "<li><a href='/elfstatus'>Elf status</a></li>\n"
  "<li><a href='/doc'>Sensor data</a></li>\n"
  "</ul></p>\n";
const char* pageFooter =
  "\n<p><a href='/'>WaterElf</a>&nbsp;&nbsp;&nbsp;"
// no spiffs support at present
// "<a href='/edit'>SPIFFS file editor</a>&nbsp;&nbsp;&nbsp;"
//  "<a href='/waterelfoverview.html'>Documentation</a>&nbsp;&nbsp;&nbsp;"
  "<a href='https://gitlab.com/hamishcunningham/unphone/tree/master/waterelf/'>Documentation</a>&nbsp;&nbsp;&nbsp;"
  "</p></body></html>";

/////////////////////////////////////////////////////////////////////////////
// data monitoring stuff ////////////////////////////////////////////////////
const boolean SEND_DATA = true;  // turn off posting of data if required here
const int MONITOR_POINTS = 60; // number of data points to store
typedef struct {
  unsigned long timestamp;
  float waterCelsius;
  float airCelsius;
  float airHumid;
  uint16_t lux;
  float pH;
  long waterLevel1; // should be an array
  long waterLevel2;
  long waterLevel3;
  float analog;
} monitor_t;
monitor_t monitorData[MONITOR_POINTS];
int monitorCursor = 0;
int monitorSize = 0;
const int DATA_ENTRIES = 4; // size of /doc rpt; must be <= MONITOR_POINTS
void updateSensorData(monitor_t *monitorData);
void postSensorData(monitor_t *monitorData);
void printMonitorEntry(monitor_t m, String* buf);
void formatMonitorEntry(monitor_t *m, String* buf, bool JSON);

/////////////////////////////////////////////////////////////////////////////
// misc utils ///////////////////////////////////////////////////////////////
void ledOn();
void ledOff();
String ip2str(IPAddress address);
#define dbg(b, s)       if(b) Serial.print(s)
#define dbf(b, ...)     if(b) Serial.printf(__VA_ARGS__)
#define dln(b, s)       if(b) Serial.println(s)
#define startupDBG      true
#define valveDBG        true
#define monitorDBG      false
#define netDBG          true
#define miscDBG         false
#define citsciDBG       false
#define analogDBG       false
#define otaDBG          true

/////////////////////////////////////////////////////////////////////////////
// temperature sensor stuff /////////////////////////////////////////////////
#include <OneWire.h>
#include <DallasTemperature.h>
OneWire ds(14); // DS1820 pin (a 4.7K resistor is necessary)
DallasTemperature tempSensor(&ds);  // pass through reference to library
void getTemperature(float* waterCelsius);
boolean GOT_TEMP_SENSOR = false; // we'll change later if we detect sensor
DeviceAddress tempAddr; // array to hold device address

/////////////////////////////////////////////////////////////////////////////
// humidity sensor stuff ////////////////////////////////////////////////////
#include "DHT.h"
// Tinsley version: DHT dht(21, DHT22); AWEC: DHT dht(33, DHT22);
DHT dht(33, DHT22); // what digital pin we're on, plus type DHT22 aka AM2302
boolean GOT_HUMID_SENSOR = false;  // we'll change later if we detect sensor

/////////////////////////////////////////////////////////////////////////////
// light sensor stuff ///////////////////////////////////////////////////////
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2591.h>
Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // sensor id
boolean GOT_LIGHT_SENSOR = false; // we'll change later if we detect sensor

/////////////////////////////////////////////////////////////////////////////
// pH sensor stuff //////////////////////////////////////////////////////////
byte pH_Add = 0x4C;  // default value,  over written with getconfig
int pH7Cal = 2048; // default value,  over written with getconfig
int pH4Cal = 1286; // default value,  over written with getconfig
float pHStep = 59.16; // default value,  over written with getconfig
const float vRef = 4.096; // our vRef into the ADC wont be exact
// since you can run VCC lower than Vref its best to measure and adjust here
const float opampGain = 5.25; //what is our Op-Amps gain (stage 1)
boolean GOT_PH_SENSOR = false; // we'll change later if we detect sensor
int getpHraw();
String pHHealth = "Unknown!";

/////////////////////////////////////////////////////////////////////////////
// RC switch stuff //////////////////////////////////////////////////////////
#include <RCSwitch.h>
RCSwitch mySwitch = RCSwitch();
const int RCSW_CHANNEL = 2; // which 433 channel to use (I-IV)
const int RCSW_HEATER = 2;  // which 433 device to switch (1-4)

/////////////////////////////////////////////////////////////////////////////
// valve stuff //////////////////////////////////////////////////////////////
const int valvePins[] = { 32, 27, A0 }; // pins for valves
const int solPins[]   = { 21, 15, A5 }; // pins for solenoid
const int valvePinsUsed = sizeof(valvePins)/sizeof(int); // len of valvePins
const int solPinsUsed   = sizeof(solPins)/sizeof(int);   // len of solPins

/////////////////////////////////////////////////////////////////////////////
// level sensing stuff //////////////////////////////////////////////////////
const int LEVEL_TRIG_PIN=25;
const int LEVEL_ECHO_PIN1=36;
const int LEVEL_ECHO_PIN2=39;
const int LEVEL_ECHO_PIN3=34;
boolean GOT_LEVEL_SENSOR = false;  // we'll change later if we detect sensor

/////////////////////////////////////////////////////////////////////////////
// analog sensor stuff //////////////////////////////////////////////////////
#include "EmonLib.h" // Emon Library, see openenergymonitor.org
String ANALOG_SENSOR_NONE = "analog_none";
String ANALOG_SENSOR_MAINS = "analog_mains";
String ANALOG_SENSOR_PRESSURE = "analog_pressure";
String analogSensor = ANALOG_SENSOR_NONE; // default value,  over written with getconfig
EnergyMonitor emon1; // instance of energy monitor, for mains current sensor

/////////////////////////////////////////////////////////////////////////////
// valves and flow control //////////////////////////////////////////////////
// Now a Valve include a pump and a solenoid.
class Valve { // each valve /////////////////////////////////////////////////
    public:
      static int counter;       // counter for setting valve number
      int number;               // id of this valve, counting from 1

      enum valve_state_t {      // what state the valve is in
        valve_fill = 0, valve_hold, valve_drain, valve_wait, valve_unset
      };
      valve_state_t state = valve_unset;
      Valve() { number = counter++; }
    
    // try integer state: LOW: on, HIGH: off
    void valveStateChange(int valveState) {
      if(valveState == 0) {                     // fill
        digitalWrite(valvePins[number-1], HIGH);
        digitalWrite(solPins[number-1],   LOW);
      } else if(valveState == 1) {              // hold
        digitalWrite(valvePins[number-1], LOW);
        digitalWrite(solPins[number-1],   LOW);
      } else if(valveState == 2) {              // drain
        digitalWrite(valvePins[number-1], LOW);
        digitalWrite(solPins[number-1],   HIGH);
      } else if(valveState == 3) {              // wait
        digitalWrite(valvePins[number-1], LOW);
        digitalWrite(solPins[number-1],   LOW);
      } else {                                  // oops!
        Serial.println("invalid valve state");
      }
    }

    void fill()  { valveStateChange(0); }  // fill time:  red LED
    void hold()  { valveStateChange(1); }  // hold time:  no LED
    void drain() { valveStateChange(2); }  // drain time: blue LED
    void wait()  { valveStateChange(3); }  // wait time:  no LED

};
int Valve::counter = 1; // definition (the above only declares)

class FlowController  // the set of valves and their config ////////////////
{
  public:
  int numValves = valvePinsUsed; // WARNING! call init if resetting!
  Valve* valves = 0;             // the valves and their states
  bool firstTimeThrough = true;  // first time stepping since boot
  unsigned long lastChange;      // when we last swapped filling valve
  Valve vCurr;                   // currently filling
  Valve vNext;                   // next one for filling
  Valve vPrev;                   // previously filling
  
  void init() {
    Valve::counter = 1;
    if(valves != 0)
      delete(valves);
    valves = new Valve[numValves];

    vCurr = valves[0];
    vNext = valves[1];
    vPrev = valves[2];
    dbf(valveDBG,
      "vCurr.number=%u, vNext.number=%u, vPrev.number=%u\n",
      vCurr.number, vNext.number, vPrev.number);
  }

  enum controller_state_t {     // what time slice the controller is in
    controller_first = 0, controller_second, controller_third
  };
  controller_state_t cState = controller_first;
  unsigned long AIR_EX_DUR =     1000 * 10;
  unsigned long AIR_FILL_DUR =   1000 * 14 * 60;
  unsigned long WATER_FILL_DUR = 1000 * 15 * 60;

  void step() {
    unsigned long timeNow = millis();
    uint32_t freeHeap = ESP.getFreeHeap();

    if(firstTimeThrough) {
      lastChange = timeNow;
      firstTimeThrough = false;

      vCurr.fill();
      vPrev.drain();
      cState = controller_first;
      dln(valveDBG, "first time through controller");
      dbf(valveDBG,
        "timeNow=%u, lastChange=%u, cState=%d, vCurr.number=%d, heap=%d\n",
        timeNow, lastChange, cState, vCurr.number, freeHeap);
      dln(valveDBG, "entering first controller state");
      return;
    }

    unsigned long sinceLast = timeNow - lastChange;
    if(cState == controller_first && sinceLast >= AIR_EX_DUR) {
      cState = controller_second;
      vPrev.wait();
      dbf(valveDBG,
        "timeNow=%u, lastChange=%u, cState=%d, vCurr.number=%d, heap=%d\n",
        timeNow, lastChange, cState, vCurr.number, freeHeap);
      dln(valveDBG, "entering second controller state");
    } else if(cState == controller_second && sinceLast >= AIR_FILL_DUR) {
      cState = controller_third;
      vCurr.hold();
      dbf(valveDBG,
        "timeNow=%u, lastChange=%u, cState=%d, vCurr.number=%d, heap=%d\n",
        timeNow, lastChange, cState, vCurr.number, freeHeap);
      dln(valveDBG, "entering third controller state");
    } else if(cState == controller_third && sinceLast >= WATER_FILL_DUR) {
      lastChange = timeNow;
      dbf(valveDBG,
        "before: vCurr.number=%u, vNext.number=%u, vPrev.number=%u\n",
        vCurr.number, vNext.number, vPrev.number);
      Valve vTemp = vPrev;
      vPrev = vCurr;
      vCurr = vNext;
      vNext = vTemp;
      dbf(valveDBG,
        "after:  vCurr.number=%u, vNext.number=%u, vPrev.number=%u\n",
        vCurr.number, vNext.number, vPrev.number);

      vCurr.fill();
      vPrev.drain();
      cState = controller_first;
      dbf(valveDBG,
        "timeNow=%u, lastChange=%u, cState=%d, vCurr.number=%d, heap=%d\n",
        timeNow, lastChange, cState, vCurr.number, freeHeap);
      dln(valveDBG, "entering first controller state (2)");
      Serial.println();
    }
  }
};

FlowController flowController;

/////////////////////////////////////////////////////////////////////////////
// config utils /////////////////////////////////////////////////////////////
template<typename G> G getConfig(const char * configName);
template<typename S> bool setConfig(const char * configName, S configData);
void loadPersistentConfig();

/////////////////////////////////////////////////////////////////////////////
// setup ////////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(ELF_SERIAL_SPEED);
  Serial.printf("Serial initialised at %d\n", ELF_SERIAL_SPEED);
  esp_log_level_set("*", ESP_LOG_ERROR);

  pinMode(BUILTIN_LED, OUTPUT); // turn built-in LED on
  blink(3);                     // signal we're starting setup
  Serial.printf("WaterElf: firmware is at version %d\n", firmwareVersion);

  // read persistent config
  if(SPIFFS.begin(true)) // true triggers format if flash unpartitioned
    loadPersistentConfig();
  else
    dln(startupDBG, "spiffs begin failed, no config loaded!");
  
  // start the sensors
  startPeripherals();

  // do wifi provisionning, start the web server
  Serial.printf("doing wifi manager\n");
  joinmeManageWiFi(apSSID, _ELF_AP_KEY);
  Serial.printf("wifi manager done\n\n");
  printIPs();
  startWebServer();

  // initialise the valve pins and the flow controller
  dln(startupDBG, "doing flow controller and valve init....");
  flowController.init();
  for(int i = 0; i < valvePinsUsed; i++) {
    pinMode(valvePins[i], OUTPUT);
    pinMode(solPins[i], OUTPUT);
  }

  // check for and perform firmware updates as needed
  vTaskDelay(2000 / portTICK_PERIOD_MS);  // let wifi settle
  joinmeOTAUpdate( // download via raw url; for private repo use _GITLAB_TOKEN
    firmwareVersion, _GITLAB_PROJ_ID, "", "waterelf%2Ffirmware%2F"
  );

  delay(300); blink(3);                   // signal we've finished config
  printf("\n"); delay(500); printf("\n");
}

/////////////////////////////////////////////////////////////////////////////
// looooooooooooooooooooop //////////////////////////////////////////////////
void loop() {
  if(reboot==true) {
    SPIFFS.end();
    ESP.restart();
  }

  flowController.step(); // set valves on and off etc.

  if(loopCounter == TICK_MONITOR) { // monitor levels, push data
    monitor_t* now = &monitorData[monitorCursor];
    if(monitorSize < MONITOR_POINTS)
      monitorSize++;
    now->timestamp = millis();
    if(GOT_TEMP_SENSOR)
      getTemperature(&now->waterCelsius);               
    if(GOT_HUMID_SENSOR)
      getHumidity(&now->airCelsius, &now->airHumid);
    if(GOT_LIGHT_SENSOR)
      getLight(&now->lux);
    if(GOT_PH_SENSOR)
      getPH(&now->pH);
    if(GOT_LEVEL_SENSOR) {
      getLevel(LEVEL_ECHO_PIN1, &now->waterLevel1);     
      getLevel(LEVEL_ECHO_PIN2, &now->waterLevel2);     
      getLevel(LEVEL_ECHO_PIN3, &now->waterLevel3);     
      // dbg(valveDBG, "wL1: "); dbg(valveDBG, now->waterLevel1);
      // dbg(valveDBG, "; wL2: "); dbg(valveDBG, now->waterLevel2);
      // dbg(valveDBG, "; wL3: "); dln(valveDBG, now->waterLevel3);
    }
    if(analogSensor!=ANALOG_SENSOR_NONE) getAnalog(&now->analog);
    
    if(SEND_DATA)
      postSensorData(&monitorData[monitorCursor]);  // push data to the cloud

    if(++monitorCursor == MONITOR_POINTS)
      monitorCursor = 0;
  }

  if(loopCounter == TICK_WIFI_DEBUG) {
    // ? a way to only trigger on less loops printIPs();
  }
  if(loopCounter == TICK_HEAP_DEBUG) {
    dbg(miscDBG, "free heap="); dln(miscDBG, ESP.getFreeHeap());
  }

  if(loopCounter++ == LOOP_ROLLOVER)
    loopCounter = 0;
}

/////////////////////////////////////////////////////////////////////////////
// wifi and web server management stuff /////////////////////////////////////
void printIPs() {
  dbg(startupDBG, "AP SSID: ");
  dbg(startupDBG, apSSID);
  dbg(startupDBG, "; IP address(es): local=");
  dbg(startupDBG, WiFi.localIP());
  dbg(startupDBG, "; AP=");
  dln(startupDBG, WiFi.softAPIP());
}
void startWebServer() {
  webServer.on("/",             handleRoot);
  webServer.onNotFound(handleNotFound);
  webServer.on("/wifi",         handleWifi);
  webServer.on("/elfstatus",    handleElfstatus);
  webServer.on("/serverconf",   handleServerconf);
  webServer.on("/analogconf",   handleAnalogconf);
  webServer.on("/pHconf",       handlePHconf);
  webServer.on("/wfchz",        handleWfchz);
  webServer.on("/svrchz",       handleSvrchz);
  webServer.on("/algchz",       handleAlgchz);
  webServer.on("/pHchz",        handlePHchz);
  webServer.on("/doc",         handleData);
  webServer.on("/actuate",      handleActuate);
  webServer.on("/valve1",       handleValve1);
  webServer.on("/valve2",       handleValve2);
  webServer.on("/valve3",       handleValve3);
  webServer.on("/reboot",       handleReboot);
  webServer.serveStatic("/", SPIFFS, "/");
  webServer.addHandler(
    new SPIFFSEditor(SPIFFS, SPIFFS_username, SPIFFS_password)
  );

  webServer.begin();
  dln(startupDBG, "HTTP server started");
}
void handleNotFound(AsyncWebServerRequest *request) {
  dbg(netDBG, "URI Not Found: ");
  dln(netDBG, request->url());
  // TODO send redirect to /? or just use handleRoot?
  request->send(200, "text/plain", "URI Not Found");
}
void handleRoot(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page notionally at /");
  String toSend = pageTop;
  toSend += pageTop2;
  toSend += pageDefault;
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleData(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /doc");
  String toSend = pageTop;
  toSend += ": Sensor Data";
  toSend += pageTop2;
  toSend += "\n<h2>Sensor Data</h2><p><pre>\n";

  for(
    int i = monitorCursor - 1, j = 1;
    j <= DATA_ENTRIES && j <= monitorSize;
    i--, j++
  ) {
    formatMonitorEntry(&monitorData[i], &toSend, false);
    toSend += "\n";
    if(i == 0)
      i = MONITOR_POINTS;
  }

  toSend += "</pre>\n";
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
String genAPForm() {
  String f = pageTop;
  f += ": Wifi Config";
  f += pageTop2;
  f += "<h2>Choose a wifi access point to join</h2>\n";
  f += "<h3>Signal strength in brackets, lower is better</h3><p>\n";
  
  const char *checked = " checked";

  int n = WiFi.scanNetworks();
  dbg(netDBG, "scan done: ");
  if(n == 0) {
    dln(netDBG, "no networks found");
    f += "No wifi access points found :-( ";
  } else {
    dbg(netDBG, n);
    dln(netDBG, " networks found");
    f += "<form method='POST' action='wfchz'> ";
    for(int i = 0; i < n; ++i) {
      // print SSID and RSSI for each network found
      dbg(netDBG, i + 1);
      dbg(netDBG, ": ");
      dbg(netDBG, WiFi.SSID(i));
      dbg(netDBG, " (");
      dbg(netDBG, WiFi.RSSI(i));
      dbg(netDBG, ")");
      dln(netDBG, (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");

      f.concat("<input type='radio' name='ssid' value='");
      f.concat(WiFi.SSID(i));
      f.concat("'");
      f.concat(checked);
      f.concat(">");
      f.concat(WiFi.SSID(i));
      f.concat(" (");
      f.concat(WiFi.RSSI(i));
      f.concat(" dBm)");
      f.concat("<br/>\n");
      checked = "";
    }
  }
  f += "<br/>Hidden SSID: <input type='textarea' name='hssid'><br/><br/> ";
  f += "<br/>Pass key: <input type='textarea' name='key'><br/><br/> ";
  f += "<input type='submit' value='Submit'></form></p>";
  f += "<a href='/'>Back</a><br/><a href='/wifi'>Try again?</a></p>\n";

  f += pageFooter;
  return f;
}
void handleWifi(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /wifi");
  String toSend = genAPForm();
  request->send(200, "text/html", toSend);
}

void handleElfstatus(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /elfstatus");

  String toSend = pageTop;
  toSend += ": Elf Status";
  toSend += pageTop2;
  toSend += "\n<h2>Elf Status</h2><p><ul>\n";

  toSend += "\n<li>SSID: ";
  toSend += WiFi.SSID();
  toSend += "</li>";
  toSend += "\n<li>Status: ";
  switch(WiFi.status()) {
    case WL_IDLE_STATUS:
      toSend += "WL_IDLE_STATUS</li>"; break;
    case WL_NO_SSID_AVAIL:
      toSend += "WL_NO_SSID_AVAIL</li>"; break;
    case WL_SCAN_COMPLETED:
      toSend += "WL_SCAN_COMPLETED</li>"; break;
    case WL_CONNECTED:
      toSend += "WL_CONNECTED</li>"; break;
    case WL_CONNECT_FAILED:
      toSend += "WL_CONNECT_FAILED</li>"; break;
    case WL_CONNECTION_LOST:
      toSend += "WL_CONNECTION_LOST</li>"; break;
    case WL_DISCONNECTED:
      toSend += "WL_DISCONNECTED</li>"; break;
    default:
       toSend += "unknown</li>";
  }

  toSend += "\n<li>Local IP: ";   toSend += ip2str(WiFi.localIP());
  toSend += "</li>\n";
  toSend += "\n<li>Soft AP IP: "; toSend += ip2str(WiFi.softAPIP());
  toSend += "</li>\n";
  toSend += "\n<li>AP SSID name: "; toSend += apSSID;
  toSend += "</li>\n";
  toSend += "\n<li>Data sharing server address: "; toSend += svrAddr;
  toSend += "</li>\n";
  toSend += "\n<li>Analog sensor type: "; toSend += analogSensor;
  toSend += "</li>\n";
  toSend += "\n<li>pH sensor I2C address: 0x"; toSend += String(pH_Add,HEX);
  toSend += "</li>\n";
  if(GOT_PH_SENSOR) {
    toSend += "\n<li>pH 4 calibration value: "; toSend += String(pH4Cal);
    toSend += "</li>\n";
    toSend += "\n<li>pH 7 calibration value: "; toSend += String(pH7Cal);
    toSend += "</li>\n";
    toSend += "\n<li>pH probe slope: "; toSend += String(pHStep);
    toSend += "</li>\n";
    if(pHStep >= 56.20 && pHStep < 60.35 && pH7Cal >= 2023 && pH7Cal < 2074)
      pHHealth="<font color='green'>Good</font>";
    else if(
      pHStep >= 50.29 && pHStep < 62.19 && pH7Cal >= 2023 && pH7Cal < 2074
    )
      pHHealth="<font color='orange'>Aging but still OK</font>";
    else
      pHHealth="<font color='red'>Poor - replace probe now!</font>";
    toSend += "\n<li>pH probe health: "; toSend += String(pHHealth);
    toSend += "</li>\n";
  }
  toSend += "\n<li>Elf Version: ";
  toSend += " v"; toSend += elfVersionStr;
  toSend += "</li>\n";
  toSend += "\n<li>Firmware Version: ";
  toSend += String(firmwareVersion);
  toSend += "</li>\n";

  unsigned long timeNow = millis();
  toSend += "\n<li>Time since last reboot: ";
  if(timeNow <= 120000){
    toSend += String(timeNow/1000);
    toSend += " seconds</li>\n";
  } else if(timeNow <= 7200000){
    toSend += String(timeNow/60000);
    toSend += " minutes</li>\n";
  } else {
    toSend += String(timeNow/3600000);
    toSend += " hours</li>\n";
  }
  
  toSend += "<h3>Reboot WaterElf?</h3>"; 
  toSend += "<form method='POST' action='reboot'> ";
  toSend += "<input type='submit' value='Reboot'></form></p>";
  toSend += "</ul></p>";

  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleWfchz(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /wfchz");
  String toSend = pageTop;
  toSend += ": joining wifi network";
  toSend += pageTop2;
  String ssid = "";
  String key = "";

  for(uint8_t i = 0; i < request->args(); i++ ) {
    dln(netDBG, " " + request->argName(i) + ": " + request->arg(i));
    if(request->argName(i) == "hssid" && request->arg(i) != "")
      ssid = request->arg(i);
    else if(request->argName(i) == "ssid")
      ssid = request->arg(i);
    else if(request->argName(i) == "key")
      key = request->arg(i);
  }
    dln(netDBG, " " + ssid + ": " + key);

  if(ssid == "") {
    toSend += "<h2>Ooops, no SSID...?</h2>";
    toSend += "<p>Looks like a bug :-(</p>";
// TODO    footAndRespond(toSend, request);
  } else {
    toSend += "<h2>Done! Now trying to join network...</h2>";
    toSend += "<p>Check <a href='/elfstatus'>wifi status here</a>.</p>";
// TODO    footAndRespond(toSend, request);

    char ssidchars[ssid.length()+1];
    char keychars[key.length()+1];
    ssid.toCharArray(ssidchars, ssid.length()+1);
    key.toCharArray(keychars, key.length()+1);
    WiFi.disconnect(true);
    WiFi.begin(ssidchars, keychars);
  }

  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
String genServerConfForm() {
  String f = pageTop;
  f += ": Server Config";
  f += pageTop2;
  f += "<h2>Configure data sharing</h2><p>\n";

  f += "<form method='POST' action='svrchz'> ";
  f += "<br/>Local server IP address: ";
  f += "<input type='textarea' name='svraddr'><br/><br/> ";
  //f += "Sharing on WeGrow.social: ";
  // TODO set checked dependent on getCloudShare()
  //f += "on <input type='radio' name='wegrow' value='on' checked>\n";
  //f += "off <input type='radio' name='wegrow' value='off'><br/><br/>\n";
  f += "<input type='submit' value='Submit'></form></p>";

  f += pageFooter;
  return f;
}
String genAnalogConfForm() {
  String f = pageTop;
  f += ": Analog Sensor Config";
  f += pageTop2;
  f += "<h2>Configure Analog Sensor</h2><p>\n";

  f += "<form method='POST' action='algchz'> ";
  f += "<br/>Analog sensor:\n<ul>\n";
  f += "<li>none <input type='radio' name='analog_sensor' value='analog_none' checked>\n";
  f += "<li>mains current <input type='radio' name='analog_sensor' value='analog_mains'>\n";
  f += "<li>water pressure <input type='radio' name='analog_sensor' value='analog_pressure'>\n";
  f += "<input type='submit' value='Submit'></form></ul></p>";

  f += pageFooter;
  return f;
}
String genpHConfForm() {
  String f = pageTop;
  f += ": pH Sensor Config & Calibration";
  f += pageTop2;
  f += "<h2>Configure & Calibrate pH Sensor</h2><p>\n";
  
  f += "<form method='POST' action='pHchz'> ";
  f += "<br/>pH sensor I2C address (hex): ";
  f += "<input type='textarea' name='pHAdd'><br/><br/>\n";
  if (GOT_PH_SENSOR) {
    f += "To calibrate the pH sensor, place pH probe in calibration ";
    f += "solution, wait several minutes for readings to settle, then select the relevant ";
    f += "radio button below and click submit<br/><br/>";
    f += "<li>none <input type='radio' name='ph_Cal' value='NoCal' checked>\n";
    f += "<li>pH4 Calibration <input type='radio' name='ph_Cal' value='pH4Cal'>\n";
    f += "<li>pH7 Calibration <input type='radio' name='ph_Cal' value='pH7Cal'>\n</li>"; 
  }
  f += "<br/><input type='submit' value='Submit'></form></p>";
  f += pageFooter;
  return f;
}
void handleServerconf(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /serverconf");
  String toSend = genServerConfForm();
  request->send(200, "text/html", toSend);
}
void handleAnalogconf(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /analogconf");
  String toSend = genAnalogConfForm();
  request->send(200, "text/html", toSend);
}
void handlePHconf(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /pHconf");
  String toSend = genpHConfForm();
  request->send(200, "text/html", toSend);
}
void handleSvrchz(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /svrchz");
  String toSend = pageTop;
  toSend += ": data sharing configured";
  toSend += pageTop2;

  boolean cloudShare = false;
  for(uint8_t i = 0; i < request->args(); i++) {
    if(request->argName(i) == "svraddr") {
      svrAddr = request->arg(i);
      if(svrAddr.length() == 0) // default cloud server address
        svrAddr = _CITSCI_IP;
      toSend += "<h2>Added local server config...</h2>";
      toSend += "<p>...at ";
      toSend += svrAddr;
      toSend += "</p>";
    } else if(request->argName(i) == "key") {
      if(request->arg(i) == "on")
        cloudShare = true;
    }
  }
  dln(false, cloudShare); // intended to control cloud data sharing but
                          // currently doesn't...

  // persist the config
  if(setConfig("citsciServerAddress",svrAddr)){
    toSend += "<h2>Saved to config</h2>";
  } else {
    toSend += "<h2>NOT SAVED!</h2>";
  }

  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleAlgchz(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /algchz");
  String toSend = pageTop;
  toSend += ": analog sensor configured";
  toSend += pageTop2;

  for(uint8_t i = 0; i < request->args(); i++) {
    if(request->argName(i) == "analog_sensor") { // remember/persist the type
      String argVal = request->arg(i);
      if(argVal == "analog_mains") {
        analogSensor = ANALOG_SENSOR_MAINS;
      } else if(argVal == "analog_pressure") {
        analogSensor = ANALOG_SENSOR_PRESSURE;
      } else {
        analogSensor = ANALOG_SENSOR_NONE; // the default is...
        dln(analogDBG,"no analog sensor");
      }
      toSend += "<h2>Added analog sensor config...</h2>";
      toSend += "<p>...for ";
      toSend += analogSensor;
      toSend += "</p>";

      if(setConfig("analogSensorType",analogSensor)){
        toSend += "<h2>Saved to config</h2>";
      } else {
        toSend += "<h2>NOT SAVED!</h2>";
      }
    }
  }

  toSend += pageFooter;
  dln(analogDBG, analogSensor);
  request->send(200, "text/html", toSend);
}
void handlePHchz(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /pHchz");
  String toSend = pageTop;
  toSend += ": pH sensor configured";
  toSend += pageTop2;

  for(uint8_t i = 0; i < request->args(); i++) {
    if(request->argName(i) == "pHAdd" && request->arg(i)!="") {
      pH_Add =
        16*hextoi(request->arg(i).charAt(0)) +
        hextoi(request->arg(i).charAt(1));
      toSend += "<h2>Added pH sensor I2C address...</h2>";
      toSend += "<p>...of 0x";
      toSend += request->arg(i).substring(0, 2);
      toSend += "</p>";
      setConfig("pH I2C Address",pH_Add);   // persist the config

      toSend += "<h3>Now rebooting to use new address...</h3>";
      toSend += pageFooter;
      request->send(200, "text/html", toSend);
      reboot=true;
    }
    if(request->argName(i) == "ph_Cal") {
      if(request->arg(i) == "pH4Cal") {
        pH4Cal = getpHraw();
        setConfig("pH4Cal",pH4Cal);   // persist the cal value
        pHStep = ((((vRef*(float)(pH7Cal-pH4Cal))/4096)*1000)/opampGain)/3;
        setConfig("pHStep",pHStep);  // persist the probe slope
        toSend += "<h2>Calibrated pH probe in pH 4 solution</h2>";
        toSend += "<p> Raw value: ";
        toSend += pH4Cal;
        toSend += "</p>";
      } else if(request->arg(i) == "pH7Cal") {
        pH7Cal = getpHraw();
        setConfig("pH7Cal",pH7Cal);   // persist the cal value
        pHStep = ((((vRef*(float)(pH7Cal-pH4Cal))/4096)*1000)/opampGain)/3;
        setConfig("pHStep",pHStep);  // persist the probe slope
        toSend += "<h2>Calibrated pH probe in pH 7 solution</h2>";
        toSend += "<p> Raw value: ";
        toSend += pH7Cal;
        toSend += "</p>";
      } else if(request->arg(i) != "noCal") {
        dln(netDBG, "unknown cal response!");
        toSend += "<h2>Not recalibrated</h2>";
      }
    }
  }

  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleActuate(AsyncWebServerRequest *request) {
  dln(netDBG, "serving page at /actuate");
  String toSend = pageTop;
  toSend += ": Setting Actuator";
  toSend += pageTop2;

  boolean newPumpState = false;
  for(uint8_t i = 0; i < request->args(); i++ ) {
    if(request->argName(i) == "state") {
      if(request->arg(i) == "on")
        newPumpState = true;
    }
  }

  // now we trigger the 433 transmitter
  if(newPumpState == true){
    mySwitch.switchOn(RCSW_CHANNEL, RCSW_HEATER);
    dln(netDBG, "Actuator on");
  } else {
    mySwitch.switchOff(RCSW_CHANNEL, RCSW_HEATER);
    dln(netDBG, "Actuator off");
  }

  toSend += "<h2>Actuator triggered</h2>\n";
  toSend += "<p>(New state should be ";
  toSend += (newPumpState) ? "on" : "off";
  toSend += ".)</p>\n";
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleValve1(AsyncWebServerRequest *request) { // valves in the UI...
  handleValve(request,0);
}
void handleValve2(AsyncWebServerRequest *request) { // ...are from 1, but...
  handleValve(request,1);
}
void handleValve3(AsyncWebServerRequest *request) { // ...from 0 in FlowCon
  handleValve(request,2);
}
void handleValve(AsyncWebServerRequest *request, int valveNum) {
  dbg(valveDBG, "serving page at /valve");
  dln(valveDBG, valveNum + 1);
  String toSend = pageTop;
  toSend += ": Setting Water Valve ";
  toSend += valveNum + 1;
  toSend += pageTop2;

  String newValveState = "you shouldn't see this!";
  for(uint8_t i = 0; i < request->args(); i++ ) {
    if(request->argName(i) == "state") {
      if(request->arg(i) == "Pon") {
        newValveState = "air pump on";
        flowController.valves[valveNum].valveStateChange(0);
      } else if (request->arg(i) == "Eon") {
        newValveState = "air exhaust on";
        flowController.valves[valveNum].valveStateChange(2);
      } else if (request->arg(i) == "Boff") {
        newValveState = "both air pump & exhaust off";
        flowController.valves[valveNum].valveStateChange(1);
      }
    }
  }

  toSend += "<h2>Water Valve ";
  toSend += valveNum + 1;
  toSend += " triggered</h2>\n";
  toSend += "<p>(Now you should find ";
  toSend += newValveState;
  toSend += ".)</p>\n";
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
void handleReboot(AsyncWebServerRequest *request) {
  String toSend = pageTop;
  toSend += "<h1>Rebooting Waterelf...</h1>";
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
  reboot=true;  
}
/*
void footAndRespond(String toSend, AsyncWebServerRequest *request) {
  toSend += pageFooter;
  request->send(200, "text/html", toSend);
}
*/


/////////////////////////////////////////////////////////////////////////////
// sensor/actuator stuff ////////////////////////////////////////////////////
void startPeripherals() {
  dln(monitorDBG, "\nstartPeripherals...");
  mySwitch.enableTransmit(4);   // RC transmitter is connected to Pin 4
  delay(100);

  taskDISABLE_INTERRUPTS();
  tempSensor.begin();     // start the onewire temperature sensor
  taskENABLE_INTERRUPTS();
  if(tempSensor.getDeviceCount()==1) {
    GOT_TEMP_SENSOR = true;
    dln(monitorDBG, "Found temperature sensor");
    taskDISABLE_INTERRUPTS();
    tempSensor.getAddress(tempAddr, 0);
    tempSensor.setResolution(tempAddr, 12); // 12 bit res (DS18B20 does 9-12)
    taskENABLE_INTERRUPTS();
  } else {
    dln(monitorDBG, "Temperature sensor FAIL");
  }

  dht.begin();    // start the humidity and air temperature sensor
  float airHumid = dht.readHumidity();
  float airCelsius = dht.readTemperature();
  if (isnan(airHumid) || isnan(airCelsius)) {
    dln(monitorDBG, "Humidity sensor FAIL");
  } else {
    GOT_HUMID_SENSOR = true;
    dln(monitorDBG, "Found humidity sensor");
  }

  // configure the level sensors
  pinMode(LEVEL_TRIG_PIN, OUTPUT);
  pinMode(LEVEL_ECHO_PIN1, INPUT);
  pinMode(LEVEL_ECHO_PIN2, INPUT);
  pinMode(LEVEL_ECHO_PIN3, INPUT);
  GOT_LEVEL_SENSOR = true;

  Wire.begin();
  byte error;
  Wire.beginTransmission(0x29);
  error = Wire.endTransmission();
  if(error==0){
    GOT_LIGHT_SENSOR = true;
    tsl.begin();  // startup light sensor
    // can change gain of light sensor on the fly, to adapt 
    // brighter/dimmer light situations
    // tsl.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
    tsl.setGain(TSL2591_GAIN_MED);       // 25x gain
    // tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
    // changing the integration time gives you a longer time over which to
    // sense light longer timelines are slower, but are good in very low light
    // situtations!
    // tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); // shortest (bright)
    tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
    // tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
    // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
    // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
    // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); // longest (dim)
  }
  
  Wire.beginTransmission(pH_Add);
  error = Wire.endTransmission();
  if(error==0){
    GOT_PH_SENSOR = true;
    dln(monitorDBG, "Found pH sensor");
  }

  if(analogSensor == ANALOG_SENSOR_MAINS) {
    emon1.current(A0, 111.1); // current: input pin, calibration
  }
}

/*
postSensorData: construct & post (via GET, to confuse), sensor data like this:
GET /collect/WaterElf-10865861 HTTP/1.1
User-Agent: WaterElf/0.000001
Host: citsci.wegrow.social:8000
Accept: application/json
Content-Type: application/json
Content-Length: 146

{ "timestamp": 39653, "airTemp": 25.90, "humidity": 49.40, "light": 8, "pH": 11.12, "waterLevel1": 257, "waterLevel2": 257, "waterLevel3": 257 }
*/
void postSensorData(monitor_t *monitorData) {

// TODO if we failed to connect last time, wait another few calls before
// trying again? to increase web liveness in disconnected devices

  dln(citsciDBG, "\npostSensorData");
  String jsonBuf = "";
  String citsciAddr = svrAddr;
  if(citsciAddr.length() == 0) // default cloud server address
    citsciAddr = _CITSCI_IP;
  formatMonitorEntry(monitorData, &jsonBuf, true);
  String envelope =
    "GET /collect/"; envelope += apSSIDStr; envelope += " HTTP/1.1\r\n";
  envelope += "User-Agent: WaterElf/0.000001\r\n";
  envelope += "Host: "; envelope += citsciAddr; envelope += ":8000\r\n";
  envelope += "Accept: application/json\r\n";
  envelope += "Content-Type: application/json\r\n";
  envelope += "Content-Length: " ;
  envelope += jsonBuf.length() + 2; // + 2 for the cr/nls
  envelope += "\r\n\r\n";
  envelope += jsonBuf;
  
  WiFiClient citsciClient;
  if(citsciClient.connect(citsciAddr.c_str(), 8000)) {
    dln(citsciDBG, "connected to citsci server; doing GET");
    citsciClient.print(envelope);
  } else {
    // try to reconnect if no server
    dbg(citsciDBG, citsciAddr);
    dln(citsciDBG, " - no citsci server, trying to reconnect wifi");
    printIPs();
    /*
    WiFi.disconnect(true);
    delay(4000);
    WiFi.begin();
    delay(8000);
    printIPs();
    */
    WiFi.mode(WIFI_MODE_NULL);
    delay(500);
    WiFi.mode(WIFI_MODE_APSTA);
    delay(500);
    WiFi.begin();
    delay(5000);
    printIPs();
  }
  citsciClient.stop();
  // dln(citsciDBG, envelope);

  dln(citsciDBG, "");
  return;
}
void formatMonitorEntry(monitor_t *m, String* buf, bool JSON) {
  if(JSON) buf->concat("{ ");
  buf->concat("~timestamp~+ ");
  buf->concat(m->timestamp);
  if(GOT_TEMP_SENSOR){
    buf->concat("^ ~waterTemp~+ ");
    buf->concat(m->waterCelsius);
    if(! JSON) buf->concat("\t\u00B0C");
  }
  if(GOT_HUMID_SENSOR){  
    buf->concat("^ ~airTemp~+ ");
    buf->concat(m->airCelsius);
    if(! JSON) buf->concat("\t\u00B0C");
    buf->concat("^ ~humidity~+ ");
    buf->concat(m->airHumid);
    if(! JSON) buf->concat("\t%RH");
  }
  if(GOT_LIGHT_SENSOR){
    buf->concat("^ ~light~+ ");
    buf->concat(m->lux);
    if(! JSON) buf->concat("\tlux");
  }
  if(GOT_PH_SENSOR){
    buf->concat("^ ~pH~+ ");
    if(! JSON) buf->concat("\t ");
    buf->concat(m->pH);
  }
  if(GOT_LEVEL_SENSOR){
    buf->concat("^ ~waterLevel1~+ "); buf->concat(m->waterLevel1);
    if(! JSON) buf->concat("\tcm");
    buf->concat("^ ~waterLevel2~+ "); buf->concat(m->waterLevel2);
    if(! JSON) buf->concat("\tcm");
    buf->concat("^ ~waterLevel3~+ "); buf->concat(m->waterLevel3);
    if(! JSON) buf->concat("\tcm");
  }
  if(analogSensor!=ANALOG_SENSOR_NONE){
    buf->concat("^ ~analog~+ ");
    buf->concat(m->analog);
    if(! JSON) buf->concat("\tanalog");
  }
  if(JSON) {
    buf->concat(" }");
    buf->replace('~', '"');
    buf->replace('^', ',');
    buf->replace('+', ':');
  } else { // remove quotes and commas
    buf->replace('~', ' ');
    buf->replace('^', '\n');
    buf->replace('+', '\t');
    buf->concat("\n");
  }
}
void getTemperature(float* waterCelsius) {
  taskDISABLE_INTERRUPTS(); 
  (*waterCelsius) = tempSensor.getTempC(tempAddr); // Read in previous temp result
  taskENABLE_INTERRUPTS();
  tempSensor.requestTemperatures(); // begin measurement ready for next time round
  dbg(monitorDBG, "Water temp: ");
  dbg(monitorDBG, *waterCelsius);
  dln(monitorDBG, " C, ");
  return;
}
void getHumidity(float* airCelsius, float* airHumid) {
  (*airCelsius) = dht.readTemperature();
  (*airHumid) = dht.readHumidity();
  dbg(monitorDBG, "Air Temp: ");
  dbg(monitorDBG, *airCelsius);
  dbg(monitorDBG, " C, ");
  dbg(monitorDBG, "Humidity: ");
  dbg(monitorDBG, *airHumid);
  dln(monitorDBG, " %RH, ");
  return;
}
void getLight(uint16_t* lux) {
  sensors_event_t event;
  tsl.getEvent(&event);
  (*lux) = event.light; 
  dbg(monitorDBG, "Light: ");
  dbg(monitorDBG, *lux);
  dln(monitorDBG, " Lux");
  return;
}
void getPH(float* pH) {
  int adc_result = getpHraw();

  // we have a our Raw pH reading from the ADC; now figure out what the pH is
  float milliVolts = (((float)adc_result/4096)*vRef)*1000;
  float temp = ((((vRef*(float)pH7Cal)/4096)*1000) - milliVolts) / opampGain;
  (*pH) = 7-(temp/pHStep); 
  dbg(monitorDBG, "pH: ");
  dbg(monitorDBG, *pH);
  dln(monitorDBG, " pH");
  return;
}
int getpHraw() {
  // this is our I2C ADC interface section
  // assign 2 BYTES variables to capture LSB & MSB (or Hi Low in this case)
  byte adc_high;
  byte adc_low;
  // we'll assemble the 2 in this variable
  int adc_result;
  Wire.requestFrom((uint8_t) pH_Add, (uint8_t) 2); // requests 2 bytes   TODO is the cast correct? done to stop compiler warning!
  while(Wire.available() < 2); // while two bytes to receive
  adc_high = Wire.read();      // set...
  adc_low = Wire.read();       // ...them
  // now assemble them, remembering byte maths; a Union works well here too
  adc_result = (adc_high * 256) + adc_low;
  return adc_result;
}
void getLevel(int echoPin, long* waterLevel) {
  long duration;
  int TIMEOUT = 15000;                          // how long to wait for pulse

  digitalWrite(LEVEL_TRIG_PIN, LOW);            // prepare for ping
  delayMicroseconds(2);
  digitalWrite(LEVEL_TRIG_PIN, HIGH);           // start ping
  delayMicroseconds(10);                        // allow 10ms ping
  digitalWrite(LEVEL_TRIG_PIN, LOW);            // stop ping
  duration = pulseIn(echoPin, HIGH, TIMEOUT);   // wait for response

  (*waterLevel) = (duration/2) / 29.1;
  delay(35);                                    // anti-interference measure

  dbg(monitorDBG, "Water Level: ");
  dbg(monitorDBG, *waterLevel);
  dln(monitorDBG, " cm, ");
  return;
}
void getAnalog(float* a) {
  dbg(analogDBG, "getAnalog of type: ");
  dln(analogDBG, analogSensor);
  if(analogSensor==ANALOG_SENSOR_NONE) {
    (*a) = 0.0;
    dln(analogDBG, "no analog sensor");

  } else if(analogSensor == ANALOG_SENSOR_MAINS) {
    (*a) = (float) ( emon1.calcIrms(1480) / 10 /*fudge!*/ );
    dbg(analogDBG, "mains reading is ");
    dbg(analogDBG, (*a)); dbg(analogDBG, "A\n");
  } else if(analogSensor == ANALOG_SENSOR_PRESSURE) {
    int analogValue = analogRead(A0);

    // conversion/"calibration" because sensor 4.5v=1.2MPa
    (*a) = (analogValue - 25) * .19;

    dbg(analogDBG, "value: ");
    dbg(analogDBG, analogValue);
    dbg(analogDBG, "    pressure: ");
    dbg(analogDBG, (*a));
    dbg(analogDBG, " PSI\n");
  }

  return;
}

/////////////////////////////////////////////////////////////////////////////
// config utils /////////////////////////////////////////////////////////////
template<typename G> G getConfig(const char * configName ) {
  File configFile = SPIFFS.open("/config.txt", FILE_READ);
  if(!configFile) {
    dln(miscDBG, "Failed to open config file for reading");
    //return false;
  }

  size_t size = configFile.size();
  if(size > 1024) {
    dln(miscDBG, "Config file size is too large");
    //return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());

  if(!json.success()) {
    dln(miscDBG, "Failed to parse config file");
    //return false;
  }

  configFile.close();
  return json[configName];
}
template< typename S> bool setConfig(const char * configName, S configData) {
  File configFileR = SPIFFS.open("/config.txt", FILE_READ);
  if (!configFileR) {
    dln(miscDBG, "Failed to open config file for reading before writing");
    return false;
  }
  
  size_t size = configFileR.size();
  if (size > 1024) {
    dln(miscDBG, "Config file size is too large");
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFileR.readBytes(buf.get(), size);
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(buf.get());
  configFileR.close();

  File configFile = SPIFFS.open("/config.txt", FILE_WRITE);
  if (!configFile) {
    dln(miscDBG, "Failed to open config file for writing");
    return false;
  }
  
  long saves = json["configUpdateCount"];
  saves ++;
  json["configUpdateCount"] = saves;
  dbg(miscDBG, "New config update count: ");
  dln(miscDBG, saves);

  json[configName] = configData;
  json.prettyPrintTo(configFile);
  configFile.close();
  return true;
}
void loadPersistentConfig() {
  dbf(startupDBG, "loading persistent config\n");
  analogSensor = getConfig<String>("analogSensorType");
  svrAddr = getConfig<String>("citsciServerAddress");
  pH_Add = getConfig<byte>("pH I2C Address");
  pH7Cal = getConfig<int>("pH7Cal");
  pH4Cal = getConfig<int>("pH4Cal");
  pHStep = getConfig<float>("pHStep");
}

/////////////////////////////////////////////////////////////////////////////
// misc utils ///////////////////////////////////////////////////////////////
void ledOn()  { digitalWrite(BUILTIN_LED, LOW); }
void ledOff() { digitalWrite(BUILTIN_LED, HIGH); }
void blink(int times) {
  ledOff();
  for(int i=0; i<times; i++) {
    ledOn(); delay(300); ledOff(); delay(300);
  }
  ledOff();
}
String ip2str(IPAddress address) {
  return
    String(address[0]) + "." + String(address[1]) + "." + 
    String(address[2]) + "." + String(address[3]);
}
char *getMAC(char *buf) { // the MAC is 6 bytes, so needs careful conversion..
  uint64_t mac = ESP.getEfuseMac(); // ...to string (high 2, low 4):
  char rev[13];
  sprintf(rev, "%04X%08X", (uint16_t) (mac >> 32), (uint32_t) mac);

  // the byte order in the ESP has to be reversed relative to normal Arduino
  for(int i=0, j=11; i<=10; i+=2, j-=2) {
    buf[i] = rev[j - 1];
    buf[i + 1] = rev[j];
  }
  buf[12] = '\0';
  return buf;
}
byte hextoi(char c) {
  if(c >= '0' && c <= '9')
    return (byte)(c - '0');
  if(c >= 'A' && c <= 'F')
    return (byte)(c-'A'+10);
  if(c >= 'a' && c <= 'f')
    return (byte)(c-'a'+10);
  return 0;
}
