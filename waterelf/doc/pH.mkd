## Welcome to WaterElf
### A helpful device for monitoring and controlling aquaponics systems.

#### pH sensor

The pH probe should be placed in the fishtank, with the bottom of the sensor in water continuously, but with the cable and first 2cm of the probe out of the water. This probe needs cleaning every week as well as calibrating monthly for accurate results. Also the probes are consumable---the probe will need replacing every 6-12 months.

![](pHprobe.jpg)
![](MinipH2_tnd.jpg)
  
Replacement pH probes are readily available with a standard BNC connector. We use a sub-module from Sparky's Widgets (details [here](https://github.com/SparkysWidgets/MinipHHW)) to accurately amplify and digitize the tiny voltages created by the pH probe. They do a fantastic job of explaining pH, how pH probes work and how to design and build electronics to measure it [here](https://www.sparkyswidgets.com/portfolio-item/ph-probe-interface/).

Because of the way the probes are constructed, the sensor end **must** remain wet at all times; if the sensor 'bulb' dries out then it is fatally damaged. However, although the junction where the cable emerges *seems* waterproof, we've found that most probes are not suitable for complete immersion in water and fail after only a few weeks. Therefore the pH probe needs to be arranged so that it is half in, half out of the water.

The probe should be cleaned approximately every week or so, more often if it seems visibly dirty after a week, less often if it still seems very clean. It is considered good practice to clean and calibrate the pH probe every month (or more), again if you observe very little difference in measurement after calibration then you can safely increase the interval before you re-calibrate; but equally, if the readings have changed more than (approximately) 0.3 then consider re-calibrating more often.

To calibrate, you will need to have two known solutions; a pH4 calibration solution and one other (higher) value of pH calibration solution (typically pH7 is used). Clean the pH probe before beginning calibration. You should ensure that both solutions are at the same temperature, and for the highest accuracy, place the temperature probe into the calibration solutions along with the pH probe. Place both probes in some pH4 solution, and wait until the pH reading settles. This reading will show the error that has accumulated since the last calibration. Calibrate the probe at this pH, then rinse the probes in tap water. Place both probes into the pH7 (or higher) solution and calibrate again. Finally rinse the probes before returning them to the fishtank.

The pH amplifier and ADC module is connected to the waterelf via I2C, with an address ranging from 0x4A to 0x4F.
