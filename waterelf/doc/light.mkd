## Welcome to WaterElf
### A helpful device for monitoring and controlling aquaponics systems.

#### Light sensor

A light sensor is integrated into the transparent lid of the waterelf and records light levels in lux.

![](lightsensor.jpg)
![](lightsensorinsidelid.jpg)

We use a breakout board by Adafruit for the TSL2591 sensor (details [here](https://learn.adafruit.com/adafruit-tsl2591)). The sensor has a very wide response range from total darkness through to intense sunlight [datasheet](http://ams.com/documents/20143/36005/TSL2591_DS000338_6-00.pdf). The connection to the sensor board has a pin missing and a corresponding blocked hole on the cable connector to ensure it is connected the right way round.

The light sensor is connected to the waterelf via I2C, with an address of 0x29.
