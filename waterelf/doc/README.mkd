Welcome to WaterElf  
_An Overview_
----------------------------

### A helpful and open device for monitoring and controlling aquaponics systems.

Hardware: WaterElf v6  
Firmware: v1.17

This waterelf has a number of sensors and controllers, some built-in and some connected by cables. Together with a microcontroller, it monitors the environment and controls the flooding and draining of three growbeds. Data is usually sent to a cloud server for storage but the system can operate without internet connectivity.

#### [Water Temperature](watertemp.mkd)
The water temperature sensor is fully submersible and should be placed in the fish tank.

#### [Air Temperature & Humidity](airtemp.mkd)
This sensor is **not** waterproof, it can be placed anywhere in the growing area but away from water splashes.

#### [Growbed Level](growlevel.mkd)
This sensor is designed to be mounted onto a tube set into the growbed and free of media. It needs to be mounted at least 20cm above the maximum water level as it cannot measure distances below this. The exact distance the sensor is mounted above the bottom of the tank can be configured. The sensor is waterproof but care should still be taken not to leave it immersed in water. The sensor plugs into the valve controller, which connects in turn to the waterelf

#### [pH](pH.mkd)
The pH probe should be placed in the fishtank, with the bottom of the sensor in water continuously, but with the cable and first 2cm of the probe out of the water. This probe will need cleaning weekly, calibrating monthly and replacing every 6-12 months for accurate results.

#### Power
The waterelf needs 5V / 1A power from a standard phone-type charger (micro USB).

#### [Light](light.mkd)
A light sensor is integrated into the transparent lid of the waterelf and records light levels in lux.

#### [Growbed Valves](valve.mkd)
The waterelf can control 3 pnuematic actuators that in turn operate the growbed valves. Each actuator is an air pump and valve that operate together to pressurise a chamber around a flexible rubber tube carrying the water. When this chamber is pressurised it flattens the flexible tube, so preventing water from flowing. When the valve is operated and the chamber pressure relased, the flexible tube returns to it's open state and water flows freely.

#### Web Control
The waterelf has it's own built in wifi access point and webserver. It creates an access point during the boot process if it cannot join a wifi network. You can use this to [configure the waterelf to use your internet connection with your wifi details](/README.mkd#joinme-ap-based-wifi-provisioning-and-ota). You can also use the web based controls to operate the valves and [view the sensor data](/doc). You can also configure and calibrate the pH sensor and the growbed level sensors.

This project is open source, so please send details of your modifications to us so that everyone can benefit from your work in turn. 
