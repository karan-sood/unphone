// UIController.cpp

#include "AllUIElement.h"

// initialisation flag, not complete until parent has finished config
bool UIController::provisioned = false;

// the UI elements types (screens) /////////////////////////////////////////
char *ui_mode_names[] = {
  "Menu",
  "Testcard: basic graphics",
  "Touchpaint",
  "Predictive text",
  "Configure wifi",
  // not finished: "Qwerty keyboard",
};
uint8_t NUM_UI_ELEMENTS = 5;  // number of UI elements

// instantiate the display and touch screen ////////////////////////////////
Adafruit_HX8357 tft = Adafruit_HX8357(
  IOExpander::LCD_CS, IOExpander::LCD_DC, IOExpander::LCD_RESET
);
Adafruit_STMPE610 ts = Adafruit_STMPE610(IOExpander::TOUCH_CS);

// keep Arduino IDE compiler happy /////////////////////////////////////////
UIElement::UIElement(Adafruit_HX8357* tft, Adafruit_STMPE610* ts) { 
  m_tft = tft;
  m_ts = ts; 
}
void UIElement::someFuncDummy() { }

// constructure for the main class /////////////////////////////////////////
UIController::UIController(ui_modes_t start_mode) {
  m_mode = start_mode;
}

bool UIController::begin() { ////////////////////////////////////////////////
  D("UI.begin()\n")
  IOExpander::begin();

  bool status = ts.begin();
  if(!status) {
    E("failed to start touchscreen controller\n");
  } else {
    D("worked\n");
  }

  IOExpander::digitalWrite(IOExpander::BACKLIGHT, LOW);
  tft.begin(HX8357D);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, HIGH);
  tft.setTextWrap(false);

  if (!SD.begin(IOExpander::SD_CS)) {
    E("SD.begin failed\n")
  } else {
    D("SD.begin OK\n");
  } 

  tft.fillScreen(HX8357_GREEN);
  WAIT_MS(50)
  tft.fillScreen(HX8357_BLACK);
  
  // define the menu element and the first m_element here 
  m_menu = new MenuUIElement(&tft, &ts);
  allocateUIElement(m_mode);

  redraw();
  return status;
}

UIElement* UIController::allocateUIElement(ui_modes_t newMode) {
  if(m_element != 0 && m_element != m_menu) delete(m_element);

  switch(newMode) {
    // not finished:
    // case ui_qwerty:
    //   m_element = new QwertyKeyboardUIElement(&tft, &ts);  break;
    case ui_menu:
      m_element = m_menu;                                  break;
    case ui_configure:
      m_element = new ConfigUIElement(&tft, &ts);          break;
    case ui_testcard:
      m_element = new TestCardUIElement(&tft, &ts);        break;
    case ui_touchpaint:
      m_element = new TouchpaintUIElement(&tft, &ts);      break;
    case ui_text:
      m_element = new TextPageUIElement(&tft, &ts);        break;
    default:
      Serial.printf("invalid UI mode %d in allocateUIElement\n", newMode);
      m_element = m_menu;
  }

  return m_element;
}

// touch management code ////////////////////////////////////////////////////
TS_Point nowhere(-1, -1, -1);    // undefined coordinate
TS_Point firstTouch(0, 0, 0);    // the first touch defaults to 0,0,0
TS_Point p(-1, -1, -1);          // current point of interest (signal)
TS_Point prevSig(-1, -1, -1);    // the previous accepted touch signal
bool firstTimeThrough = true;    // first time through gotTouch() flag
uint16_t fromPrevSig = 0;        // distance from previous signal
unsigned long now = 0;           // millis
unsigned long prevSigMillis = 0; // previous signal acceptance time
unsigned long sincePrevSig = 0;  // time since previous signal acceptance
uint16_t DEFAULT_TIME_SENSITIVITY = 150; // min millis between touches
uint16_t TIME_SENSITIVITY = DEFAULT_TIME_SENSITIVITY;
uint16_t DEFAULT_DIST_SENSITIVITY = 200; // min distance between touches
uint16_t DIST_SENSITIVITY = DEFAULT_DIST_SENSITIVITY;
uint16_t TREAT_AS_NEW = 600;     // if no signal in this period treat as new
uint8_t MODE_CHANGE_TOUCHES = 1; // number of requests needed to switch mode
uint8_t modeChangeRequests = 0;  // number of current requests to switch mode

void setTimeSensitivity(uint16_t s = DEFAULT_TIME_SENSITIVITY) { ////////////
  TIME_SENSITIVITY = s;
}
void setDistSensitivity(uint16_t d = DEFAULT_DIST_SENSITIVITY) { ////////////
  DIST_SENSITIVITY = d;
}
uint16_t distanceBetween(TS_Point a, TS_Point b) { // coord distance ////////
  uint32_t xpart = b.x - a.x, ypart = b.y - a.y;
  xpart *= xpart; ypart *= ypart;
  return sqrt(xpart + ypart);
}
void dbgTouch() { // print current state of touch model /////////////////////
  if(touchDBG) {
    D("p(x:%04d,y:%04d,z:%03d)", p.x, p.y, p.z)
    D(", now=%05lu, sincePrevSig=%05lu, prevSig=", now, sincePrevSig)
    D("p(x:%04d,y:%04d,z:%03d)", prevSig.x, prevSig.y, prevSig.z)
    D(", prevSigMillis=%05lu, fromPrevSig=%05u", prevSigMillis, fromPrevSig)
  }
}

// accept or reject touch signals ///////////////////////////////////////////
bool UIController::gotTouch() { 
  if(!ts.touched()) {
    return false; // no touches
  }
    
  // set up timings
  now = millis();
  if(firstTimeThrough) {
    sincePrevSig = TIME_SENSITIVITY + 1;
  } else {
    sincePrevSig = now - prevSigMillis;
  }

  // retrieve a point
  p = ts.getPoint();
  // TODO should we read the rest of the buffer? 
  //  while (! touch.bufferEmpty()) 

  // if it is at 0,0,0 and we've just started then ignore it
  if(p == firstTouch && firstTimeThrough) {
    dbgTouch();
    if(touchDBG) D(", rejecting (0)\n\n")
    return false;
  }
  firstTimeThrough = false;
  
  // calculate distance from previous signal
  fromPrevSig = distanceBetween(p, prevSig);
  dbgTouch();

  if(touchDBG)
    D(", sincePrevSig<TIME_SENS.: %d...  ", sincePrevSig<TIME_SENSITIVITY)
  if(sincePrevSig < TIME_SENSITIVITY) { // ignore touches too recent
    if(touchDBG) D("rejecting (2)\n")
  } else if(
    fromPrevSig < DIST_SENSITIVITY && sincePrevSig < TREAT_AS_NEW
  ) {
    if(touchDBG) D("rejecting (3)\n")
  } else {
    prevSig = p;
    prevSigMillis = now;
    D("decided this is a new touch\n")
    return true;
  }
  return false;
}

/////////////////////////////////////////////////////////////////////////////
void UIController::changeMode() {
  D("changing mode from %d to...", m_mode)
  tft.fillScreen(HX8357_BLACK);
  setTimeSensitivity(); // set TIME_SENS to the default

  // allocate an element according to nextMode and 
  if(m_mode == ui_menu) {       // coming OUT of menu
    D("...%d\n", nextMode)
    int8_t menuSelection = ((MenuUIElement *)m_menu)->getMenuItemSelected();
    if(menuSelection != -1)     // if user selected an item use it
      nextMode = (ui_modes_t) menuSelection; // (else use current nextMode)

    if(nextMode == ui_touchpaint)
      setTimeSensitivity(25);   // ? make class member and move to TPUIE

    m_mode =    nextMode;
    m_element = allocateUIElement(nextMode);
  } else {                      // going INTO menu
    D("...%d (menu)\n", ui_menu)

    modeCounter = ++modeCounter % NUM_UI_ELEMENTS; // calculate next mode
    if(modeCounter == 0) modeCounter++; // wrap through to config at end
    nextMode = (ui_modes_t) modeCounter;
    dbf(miscDBG, "nextMode=%d, modeCounter=%d\n", nextMode, modeCounter);

    m_mode =    ui_menu;
    m_element = m_menu;
  }

  redraw();
  return;
}

/////////////////////////////////////////////////////////////////////////////
void UIController::handleTouch() {
  p.x = map(p.x, TS_MAXX, TS_MINX, tft.width(), 0);
  p.y = map(p.y, TS_MAXY, TS_MINY, 0, tft.height());
  // previously, before screen rotation in unphone spin 4, we did it like
  // this (which is probably from the Adafruit example):
  // p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
  // p.y = map(p.y, TS_MINY, TS_MAXY, 0, tft.height());
  
  // TODO dump old modeChangeRequests?
  if(m_element->handleTouch(p.x, p.y)) {
    if(++modeChangeRequests >= MODE_CHANGE_TOUCHES) {
      changeMode();
      modeChangeRequests = 0;
    }
  } 
}

/////////////////////////////////////////////////////////////////////////////
void UIController::run() {
  if(gotTouch())
    handleTouch();
  m_element->runEachTurn();
}

// turn vibration motor on or off ///////////////////////////////////////////
void UIController::vibemotor(bool on) { 
  if(on)
    ts.writeRegister8(0x10, 8);   // set GPIO3 HIGH to start vibe
  else
    ts.writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
}
    
// turn the backlight on or off /////////////////////////////////////////////
void UIController::backlight(bool on) { 
  if(on)
    digitalWrite(2, HIGH);
  else
    digitalWrite(2, LOW);
}

////////////////////////////////////////////////////////////////////////////
void UIController::redraw() {
  tft.fillScreen(HX8357_BLACK);
  m_element->draw();
}

////////////////////////////////////////////////////////////////////////////
void UIController::message(char *s) {
  tft.setCursor(0, 465);
  tft.setTextSize(2);
  tft.setTextColor(HX8357_CYAN, HX8357_BLACK);
  tft.print("                          ");
  tft.setCursor(0, 465);
  tft.print(s);
}

////////////////////////////////////////////////////////////////////////////
void UIElement::drawSwitcher(uint16_t xOrigin, uint16_t yOrigin) {
  uint16_t leftX = xOrigin;
  if(leftX == 0)
    leftX = (SWITCHER * BOXSIZE) + 8; // default is on right hand side
  m_tft->fillRect(leftX, 15 + yOrigin, BOXSIZE - 15, HALFBOX - 10, WHITE);
  m_tft->fillTriangle(
    leftX + 15, 35 + yOrigin,
    leftX + 15,  5 + yOrigin,
    leftX + 30, 20 + yOrigin,
    WHITE
  );
}

////////////////////////////////////////////////////////////////////////////
void UIElement::showLine(char *buf, uint16_t *yCursor) {
  *yCursor += 20;
  m_tft->setCursor(0, *yCursor);
  m_tft->print(buf);
}
