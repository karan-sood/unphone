#include <SPI.h>                 // the SPI bus
#include <Adafruit_GFX.h>        // core graphics library
#include <Adafruit_HX8357.h>     // tft display
#include <Wire.h>                // I²C comms on the Arduino
#include <IOExpander.h>          // unPhone's IOExpander (controlled via I²C)
#include <GP2Y1010_DustSensor.h> // Library for the Sharp dust sensor
#include <DHTesp.h>              // Library for temperature / humidity sensor
DHTesp dht;

byte I2Cadd = 0x6b;      // the I2C address of the battery management chip
byte BM_Watchdog = 0x05; // Charge Termination/Timer Control Register
byte BM_OpCon    = 0x07; // Misc Operation Control Register
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

const int GasPin = 26;
const int DustLEDPin = 32;
const int DustPin = 15;
const int dhtPin = 14;

// the LCD
#define TFT_DC   33      // this isn't on the IO expander
Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, TFT_DC, IOExpander::LCD_RESET);

GP2Y1010_DustSensor dustsensor;

void setup() {
  Serial.begin(115200);
  Serial.println("Gas and Dust sensor readings");
  Wire.setClock(100000);
  Wire.begin();
  IOExpander::begin();
  checkPowerSwitch();   // Check if the power switch is now off and if so, shutdown
  delay(10);
  tft.begin(HX8357D); // start the LCD driver chip
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH); // backlight on
  tft.fillScreen(HX8357_BLACK);
  tft.setTextSize(2);

  analogSetPinAttenuation(GasPin, ADC_11db);   // this sets ADC to 0-3.3V range
  analogSetPinAttenuation(DustPin, ADC_11db);
  dustsensor.begin(DustLEDPin, DustPin);
  dustsensor.setInputVolts(3.3);
  dustsensor.setADCbit(12);
  
  dht.setup(dhtPin, DHTesp::AM2302);
  Serial.println("DHT initiated");
}

void loop() {
  checkPowerSwitch();   // Check if the power switch is now off and if so, shutdown
  tft.fillRect(0,50,320,200, HX8357_BLACK); // clear screen ready for next display
  tft.setCursor(0,50);

  tft.println("Dust Density:");
  float dust = dustsensor.getDustDensity();
  tft.println(dust);
  Serial.println(dust);

  tft.println("Gas Reading:");
  long valr = analogRead(GasPin);
  // the 3.3V range is divided into 4096 steps, giving 0.000806V per step
  // and the original voltage is divided by a 18K/10K resistor divider (1.8)
  // so the 4096 steps corespond to 0.00145V per step of original 5V
  float volts = valr*0.00145;
  tft.print(volts);
  tft.println("V");
  Serial.print(volts);
  Serial.println("V");
  
  TempAndHumidity newValues = dht.getTempAndHumidity();
  tft.println(" T:" + String(newValues.temperature) + "C H:" + String(newValues.humidity) + "%RH");

  delay(1000);
}
void checkPowerSwitch() {
  
  uint8_t inputPwrSw = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  // Serial.print("power switch says ");
  // Serial.println(inputPwrSw);

  bool powerGood = bitRead(getRegister(BM_Status),2); // bit 2 of status register indicates if USB connected

  if (!inputPwrSw) {  // when power switch off
    if (!powerGood) { // and usb unplugged we go into shipping mode
      Serial.println("Setting shipping mode to true");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      setShipping(true);
    } else { // power switch off and usb plugged in we sleep
      Serial.println("sleeping now");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      esp_sleep_enable_timer_wakeup(1000000); // sleep time is in uSec
      esp_deep_sleep_start();
    }
  }
}

void setShipping(bool value) {
  byte result;
  if (value) {
    result=getRegister(BM_Watchdog);   // Read current state of timing register
    bitClear(result, 5);               // clear bit 5
    bitClear(result, 4);               // and bit 4
    setRegister(BM_Watchdog,result); // to disable the watchdog timer (REG05[5:4] = 00)

    result=getRegister(BM_OpCon);      // Read current state of operational register
    bitSet(result, 5);                 // Set bit 5
    setRegister(BM_OpCon,result);   // to disable BATFET (REG07[5] = 1)
  } else {
    result=getRegister(BM_Watchdog);  // Read current state of timing register
    bitClear(result, 5);               // clear bit 5
    bitSet(result, 4);                 // and set bit 4
    setRegister(BM_Watchdog,result); // to enable the watchdog timer (REG05[5:4] = 01)

    result=getRegister(BM_OpCon);     // Read current state of operational register
    bitClear(result, 5);               // Clear bit 5
    setRegister(BM_OpCon,result);    // to enable BATFET (REG07[5] = 0)
  }
}

void setRegister(byte reg, byte value) {
  write8(I2Cadd,reg, value);
}

byte getRegister(byte reg) {
  byte result;
  result=read8(I2Cadd,reg);
  return result;
}

void write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}
