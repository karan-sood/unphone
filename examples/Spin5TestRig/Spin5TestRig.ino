// Spin5TestRig.ino

#define UNPHONE_SPIN 5

#include <lmic.h>               // IBM LMIC (LoraMAC-in-C) library
#include <hal/hal.h>            // hardware abstraction for LMIC on Arduino
#include <SPI.h>                // the SPI bus
#include <Adafruit_GFX.h>       // core graphics library
#include <Adafruit_HX8357.h>    // tft display local hacked version
#include <Adafruit_STMPE610.h>  // touch screen local hacked version
#include <Wire.h>               // I²C comms on the Arduino
#include <IOExpander.h>         // unPhone's IOExpander (controlled via I²C)
#include <Adafruit_Sensor.h>    // base class etc. for sensor abstraction
#include <Adafruit_LSM303_U.h>  // the accelerometer sensor
#include "private.h" // stuff not for checking in

bool DBG = true; // debug switch

// macros for debug calls to Serial.printf, with (D) and without (DD)
// new line, and to Serial.println (DDD)
#define D(args...)   { if(DBG) { Serial.printf(args); Serial.println(); } }
#define DD(args...)  { if(DBG) Serial.printf(args); }
#define DDD(s)       { if(DBG) Serial.println(s); }

// parent for device element classes
class Chunk {
public:
  int begin();  // initialisation
  void test();  // validation
};

#define VBAT_SENSE 35

/* Assign a unique ID to accel sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, IOExpander::LCD_DC, IOExpander::LCD_RESET);
Adafruit_STMPE610 ts = Adafruit_STMPE610(IOExpander::TOUCH_CS);

// calibration data for the raw touch data to the screen coordinates
#define TS_MINX 3800
#define TS_MAXX 100
#define TS_MINY 100
#define TS_MAXY 3750

// a test set of boxes
class Menu: public Chunk {
public:
  void init() {
    Wire.setClock(100000);
    Wire.begin();
  }
};

Menu m;

byte BM_I2Cadd = 0x6b;
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

#include "SD.h"

// LoRaWAN NwkSKey, network session key
// This is the default Semtech key, which is used by the prototype TTN
// network initially.
static const PROGMEM u1_t NWKSKEY[16] = _LORA_NET_KEY;

// LoRaWAN AppSKey, application session key
// This is the default Semtech key, which is used by the prototype TTN
// network initially.
static const u1_t PROGMEM APPSKEY[16] = _LORA_APP_KEY;

// LoRaWAN end-device address (DevAddr)
// See http://thethingsnetwork.org/wiki/AddressSpace
static const u4_t DEVADDR = _LORA_DEV_ADDR ; // <-- Change this address for every node!

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }



static uint8_t mydata[] = "Hello, world - I love you!";
static osjob_t sendjob;
static osjob_t voltage_update_job;
static osjob_t os_details_job;
// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 20;

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println("netid = ");
            Serial.println(F("EV_JOINED"));
            break;
        case EV_RFU1:
            Serial.println(F("EV_RFU1"));
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
            break;
        case EV_TXCOMPLETE:
            
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
                tft.setCursor(0,40);
                tft.fillRect(0, 40, 100, 24 , HX8357_BLACK);
                tft.setTextSize(3);
                tft.setTextColor(HX8357_GREEN);
                tft.println("Packet Sent:");
            if(LMIC.dataLen) {
                // data received in rx slot after tx
                Serial.print(F("Data Received: "));
                Serial.write(LMIC.frame+LMIC.dataBeg, LMIC.dataLen);
                Serial.println();
            }
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
           
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
         default:
            Serial.println(F("Unknown event"));
            break;
    }
}


//////////////////////////////////////OS JOBS////////////////////////////////////////
void do_send(osjob_t* j){
    
    
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, mydata, sizeof(mydata)-1, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

///////////////////////////////////////////////////////////////////////////////////

// Pin mapping
  const lmic_pinmap lmic_pins = {
    .nss = IOExpander::LORA_CS,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = IOExpander::LORA_RESET,
    .dio = {39,26,LMIC_UNUSED_PIN}  // Modified to suit our connection on unphone
  };

int stage=1;
bool newSwitchReading, oldSwitchReading;

void setup() {
  pinMode( VBAT_SENSE,INPUT);
  analogReadResolution(12);  // 10 bit is 0-1023, 11 bit is 0-2047, 12 bit is 0-4095
  analogSetPinAttenuation( VBAT_SENSE, ADC_6db); // 0db is 0-1V, 2.5db is 0-1.5V, 6db is 0-2.2v, 11db is 0-3.3v

  m.init();
  Serial.begin(115200);
  Serial.println(F("Starting"));
  Serial.println(F("Version 1.01"));
  Serial.println(F("Source: more-fish/food-flows/Working-on-spin5/quick_everything_test"));
  Serial.print(F("Running on Spin "));
  int version = IOExpander::getVersionNumber(), spin;
  Serial.println(spin);
  IOExpander::begin();
  oldSwitchReading=IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,LOW);
  tft.begin(HX8357D);
  tft.fillScreen(HX8357_WHITE);
  tft.setTextSize(3);
  tft.setCursor(120,10);
  tft.setTextColor(HX8357_RED);
  tft.print("Red");  
  tft.setCursor(120,40);
  tft.setTextColor(HX8357_GREEN);
  tft.print("Green");
  tft.setCursor(120,70);
  tft.setTextColor(HX8357_BLUE);
  tft.print("Blue");
  //tft.fillRect(110, 320, 100, 40 , HX8357_BLACK);
  //tft.setCursor(120,330);
  //tft.setTextColor(HX8357_WHITE);
  //tft.print("NEXT!");
  tft.fillRect(30, 30, 70, 70 , HX8357_GREEN);
  tft.fillRect(220, 30, 70, 70 , HX8357_RED);
  tft.fillRect(30, 380, 70, 70 , HX8357_BLUE);
  tft.fillRect(220, 380, 70, 70 , HX8357_YELLOW);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  tft.setCursor(0,110);
  tft.setTextColor(HX8357_GREEN);
  if(! ts.begin()) {
    D("failed to start touchscreen controller");
    tft.setTextColor(HX8357_RED);
    tft.println(" TOUCH INIT FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {
    D("touchscreen started");
    tft.println(" TOUCH INIT OK");
  }
  ts.writeRegister8(0x17, 12);  // use GPIO2 & 3 (bcd)
  ts.writeRegister8(0x13, 12); // all gpio's as outputs to minimise power consumption on unused

  if(!accel.begin())
  {
    D("Failed to start accelerometer");
    tft.setTextColor(HX8357_RED);
    tft.println(" ACCEL INIT FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {
    D("accelerometer started");
    tft.println(" ACCEL INIT OK");
  }
  
  pinMode(12,OUTPUT);     // IR_LED pin

  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  if(!SD.begin(-1)){
    D("Card Mount Failed");
    tft.setTextColor(HX8357_RED); 
    tft.println(" SD INIT FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {
    D("SD Card started");
    tft.println(" SD INIT OK");
  }
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);


  // LMIC init
  Serial.println("doing os_init()...");
  tft.setCursor(0,206);
  tft.setTextColor(HX8357_RED);   
  tft.println(" LORA INIT FAIL");
  os_init(); // if lora fails then will stop at this init, allowing fail to be seen
  tft.setCursor(0,206);
  tft.fillRect(0, 206, 319, 24 , HX8357_WHITE);
  tft.setTextColor(HX8357_GREEN);   
  tft.println(" LORA INIT OK");
  LMIC_reset();


    // Set static session parameters. Instead of dynamically establishing a session
    // by joining the network, precomputed session parameters are be provided.
    #ifdef PROGMEM
    Serial.print("here!");
    // On AVR, these values are stored in flash and only copied to RAM
    // once. Copy them to a temporary buffer here, LMIC_setSession will
    // copy them into a buffer of its own again.
    uint8_t appskey[sizeof(APPSKEY)];
    uint8_t nwkskey[sizeof(NWKSKEY)];
    memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
    memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
    LMIC_setSession (0x1, DEVADDR, nwkskey, appskey);
    #else
    // If not running an AVR with PROGMEM, just use the arrays directly 
    LMIC_setSession (0x1, DEVADDR, NWKSKEY, APPSKEY);
    #endif

    // Set up the channels used by the Things Network, which corresponds
    // to the defaults of most gateways. Without this, only three base
    // channels from the LoRaWAN specification are used, which certainly
    // works, so it is good for debugging, but can overload those
    // frequencies, so be sure to configure the full frequency range of
    // your network here (unless your network autoconfigures them).
    // Setting up channels should happen after LMIC_setSession, as that
    // configures the minimal channel set.
    LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
    LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
    LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
    // TTN defines an additional channel at 869.525Mhz using SF9 for class B
    // devices' ping slots. LMIC does not have an easy way to define set this
    // frequency and support for class B is spotty and untested, so this
    // frequency is not configured here.

    // Disable link check validation
    LMIC_setLinkCheckMode(1);

    // Set data rate and transmit power (note: txpow seems to be ignored by the library)
    LMIC_setDrTxpow(DR_SF7,14);
        //LMIC_startJoining ();
    // Start job

    //Register OS jobs
    
    do_send(&sendjob);
  if (getRegister(BM_I2Cadd,BM_Version)!=192){
    D("Battery management IC fail");
    tft.setTextColor(HX8357_RED);   
    tft.println(" BAT MAN FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {   
    D("Battery managment ic found");
    tft.println(" BAT MAN OK");
  }

  sensors_event_t event;
  accel.getEvent(&event);
  uint32_t first =(event.acceleration.x), second = (event.acceleration.y), third = (event.acceleration.z);
  if (first==0 & second == 0 & third ==0){
    D("Accelerometer fail");
    tft.setTextColor(HX8357_RED);   
    tft.println(" ACCEL FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {   
    D("Accelerometer OK");
    tft.println(" ACCEL OK");
  }
    
  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  uint8_t cardType = SD.cardType();
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);
  if(cardType == CARD_NONE){
    D("No SD card attached");
    tft.setTextColor(HX8357_RED);   
    tft.println(" SD CARD FAIL");
    tft.setTextColor(HX8357_GREEN);
  } else {   
    D("SD card OK");
    tft.println(" SD CARD OK");
  }
  tft.setTextSize(2);
  tft.setCursor(0,340);
  tft.setTextColor(HX8357_BLUE);   
  tft.println("   SLIDE SWITCH TO STOP");
  tft.println("      VIBE, IR & TONE");
}

void loop(void) {
  // retrieve a point  
  TS_Point p = ts.getPoint();
  // scale from ~0->4000 to tft.width using the calibration #'s
  p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
  p.y = map(p.y, TS_MINY, TS_MAXY, 0, tft.height());

  digitalWrite(12,HIGH);
  ts.writeRegister8(0x10, 8);   // set GPIO3 HIGH to start vibe
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
  digitalWrite(12,LOW);
  ts.writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  delay(300);
  digitalWrite(12,HIGH);
  ts.writeRegister8(0x10, 8);   // set GPIO3 HIGH to start vibe
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
  digitalWrite(12,LOW);
  ts.writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  delay(300);
  
  if((p.x >30 & p.x<100 & p.y >30 & p.y<100)|(p.x>220 & p.x<290 & p.y >30 & p.y<100)|(p.x >30 & p.x<100 & p.y >380 & p.y<450)|(p.x>220 & p.x<290 & p.y >380 & p.y<450)) // we're in the boxes
    tft.fillCircle(p.x, p.y, 20, HX8357_BLACK);

  float voltage;
  voltage = analogRead(VBAT_SENSE);
  //Serial.println(voltage_old - voltage_new);

  voltage = (voltage/ 4095) * 4.4;
  //  tft.println(voltage);
  newSwitchReading=IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  if (newSwitchReading!=oldSwitchReading) {
    while(true){} //An empty loop that never ends.
  }
}

void setRegister(byte address, byte reg, byte value) {
  write8(address, reg, value);
}

byte getRegister(byte address, byte reg) {
  byte result;
  result=read8(address,reg);
  return result;
}

void write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}
