# "main" pseudo-component makefile

UNPHONE_INC_DIR=../../../../src
UNPHONE_PARENT_DIR=../../../../..
UNPHONE_SRC_DIR=../../../src
UNPHONE_LIB_DIR=../../../lib

CPPFLAGS += -I$(UNPHONE_INC_DIR) -I$(UNPHONE_PARENT_DIR) \
  -Wno-all -Wno-extra \
  -Wno-error=return-type -Wno-write-strings -Wno-conversion-null \
  -Wno-return-type -Wno-pointer-arith -Wno-cpp -Wno-unused-variable \

BIGDEMOIDF_LIBS := $(UNPHONE_SRC_DIR) \
  $(UNPHONE_LIB_DIR)/Adafruit-GFX-Library \
  $(UNPHONE_LIB_DIR)/Adafruit_HX8357_Library \
  $(UNPHONE_LIB_DIR)/ESPAsyncWebServer/src \
  $(UNPHONE_LIB_DIR)/AsyncTCP/src/ \
  $(UNPHONE_LIB_DIR)/OneWire \
  $(UNPHONE_LIB_DIR)/ArduinoJson/src \
  $(UNPHONE_LIB_DIR)/WiFiManager \
  $(UNPHONE_LIB_DIR)/Adafruit_STMPE610 \
  $(UNPHONE_LIB_DIR)/Adafruit_LSM303DLHC \
  $(UNPHONE_LIB_DIR)/SD/src \
  $(UNPHONE_LIB_DIR)/Adafruit_Sensor \
  $(UNPHONE_LIB_DIR)/Adafruit_VS1053_Library \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src \
  $(UNPHONE_LIB_DIR)/Adafruit_ImageReader \

#  TODO current LCD version $(UNPHONE_LIB_DIR)/Adafruit_HX8357_Library \
#  $(UNPHONE_LIB_DIR)/Adafruit_HX8357_Library__1_0_7 \

COMPONENT_SRCDIRS += $(BIGDEMOIDF_LIBS)
COMPONENT_ADD_INCLUDEDIRS := $(BIGDEMOIDF_LIBS)
